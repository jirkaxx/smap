/*
  Projekt dalkove ovladane udirny za pouziti modulu ESP8266

  Vytvoril Jiri Novotny
*/

// import knihovny webserveru
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
// import knihovny SPIFFS
#include <FS.h>
//Knihovny teploty
#include <OneWire.h>
#include <DallasTemperature.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <Wire.h>
#include "DFRobot_SHT20.h"

DFRobot_SHT20    sht20;


// prihlasovaci udaje wifi
const char* ssid = "NovNetS";
const char* password = "lovelybeer13";

// inicializace proměnných jednotlivých pinů
const int ledPinC1 = 0;
const int ledPinC2 = 2;
const int ledPinC3 = 15;
const int relayPin = 12;
const int tempPin = 13;

// globální proměnné
String setProfile;
int setTemp = 65;
int setTime = 0;
int timeDiff = 0;
int secProfile = 0;
float curTemp = 0;
float curSecondTemp = 0;
float curHum = 0;
float lastChangeTemp = 20;

bool relayState = true;
bool cRelayState = true;

String serverName = "http://udirna.jnphoto.cz/log.php";

int lastLogMillis = 0;
int lastFiveSecMillis = 0;

// Vytvoření instance web serveru
AsyncWebServer server(5005);

// vytvoření instance oneWireDS z knihovny OneWire
OneWire oneWireDS(tempPin);
// vytvoření instance senzorDS z knihovny DallasTemperature
DallasTemperature senzorDS(&oneWireDS);

String getMillisFormated() {
  int mil = millis();
  int mint = mil / 60000;
  return String(mint);
}


void logToMySql(int amill) {
  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    if (amill - lastLogMillis > 30000) {
      lastLogMillis = amill;

      WiFiClient client;

      HTTPClient http;

      Serial.print("[HTTP] begin...");
      String iro = "true";
      if (relayState == false) {
        iro = "false";
      }
      String httpRequestData = "SECRET=6b6fe1a9cbd54f4fa1dc975d1a6b5e9c&ISRELAYON=" + iro + "&DEGREE=" + curTemp + "&SETDEGREE=" + setTemp + "&HUMINIDY=" + curHum;
      String requestAddress = serverName + "?" + httpRequestData;

      if (http.begin(client, requestAddress)) {  // HTTP

        Serial.println();
        Serial.print(requestAddress);
        Serial.println();

        Serial.print("[HTTP] GET...");
        // start connection and send HTTP header

        int httpCode = http.GET();

        // httpCode will be negative on error
        if (httpCode > 0) {
          // HTTP header has been send and Server response header has been handled
          Serial.printf("[HTTP] GET... code: %d", httpCode);

          // file found at server
          if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            Serial.println();
            String payload = http.getString();
            Serial.println(payload);
          }
        } else {
          Serial.printf("[HTTP] GET... failed, error: %s", http.errorToString(httpCode).c_str());
        }

        http.end();
      } else {
        Serial.printf("[HTTP} Unable to connect");
      }
    }
  }
  else {
    Serial.println("WiFi Disconnected");
  }
}

String getTemperature() {




  String sTemp = String(curTemp);
  Serial.println(sTemp + "   " + getMillisFormated());
  return sTemp;
}

// nacteni teploty s pokusnym nastavenim diody
void loadTemperature() {

  senzorDS.requestTemperatures();
  curSecondTemp = senzorDS.getTempCByIndex(0);


  curHum = sht20.readHumidity();
  curTemp = sht20.readTemperature();



  cRelayState = relayState;

  relayCheck();
  if (relayState) {
    digitalWrite(relayPin, LOW);
  setStateDiode(2);
  }
  else {
    digitalWrite(relayPin, HIGH);
  setStateDiode(3);
  }
  if (relayState != cRelayState) {
    lastChangeTemp = curTemp;
  }
}


void SetProfileWarm() {
  setTemp = 65;
  setTime = 6.5 * 60 * 60;
}

void SetProfileCold() {
  setTemp = 40;
  setTime = 72 * 60 * 60;
}

void SetProfileCustom1() {
  setTemp = 60;
  setTime = 10 * 60 * 60;
}

void SetProfileCustom2() {
  setTemp = 60;
  setTime = 24 * 60 * 60;
}

void SetProfileCustom3() {
  setTemp = 80;
  setTime = 3 * 60 * 60;
}

void SetProfile() {
  secProfile = millis() / 1000;
  if (setProfile.equals("warm")) {
    SetProfileWarm();
    return;
  }

  if (setProfile.equals("cold")) {
    SetProfileCold();
    return;
  }
  if (setProfile.equals("cus1")) {
    SetProfileCustom1();
    return;
  }
  if (setProfile.equals("cus2")) {
    SetProfileCustom2();
    return;
  }
  if (setProfile.equals("cus3")) {
    SetProfileCustom3();
    return;
  }
}




void relayCheck() {
  if(setTemp - lastChangeTemp > 5 && setTemp - curTemp < 5.0){
    relayState = false;
    cRelayState = false;
    return;
  }
  if (curTemp < setTemp) {
    relayState = true;
    return;
  }
  relayState = false;
  
  if (abs(setTemp - curTemp) > 1.0) {
    relayState = curTemp < setTemp;
}
}

// isRelayOn do stringu
String isRelayOnString() {
  return String(relayState);
}

// nacteni nastavene teploty
String getSettedTemperature() {
  return String(setTemp);
}
String getHuminidy() {
  return String(curHum);
}

// nacteni nastavene teploty
String getTime() {
  return String(timeDiff);
}

// nacteni nastavene teploty
String getProfile() {
  return setProfile;
}


// Callback stranky index
String indexCb(const String& var) {
  Serial.println(var);
  return var;
}

// pokusne blikani
void setup() {
  Serial.begin(115200);
  pinMode(ledPinC1, OUTPUT);
  pinMode(ledPinC2, OUTPUT);
  pinMode(ledPinC3, OUTPUT);
  pinMode(relayPin, OUTPUT);

  setStateDiode(0);
  
  delay(100);

  sht20.initSHT20();                                  // Init SHT20 Sensor
  sht20.checkSHT20();                                 // Check SHT20 Sensor

  // nastaveni pinu

  // vytvoreni pripojeni k spiffs prostoru
  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // pripojeni k wifi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {    
    setStateDiode(0);
    delay(500);
    setStateDiode(1);
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  setStateDiode(2);

  // vytisteni ip adresy
  Serial.println(WiFi.localIP());



  // cesta rootu
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {

    int paramsNr = request->params();
    Serial.println(paramsNr);

    for (int i = 0; i < paramsNr; i++) {
      AsyncWebParameter* p = request->getParam(i);
      if (p->name() == "settemp") {
        setTemp = p->value().toInt();
      }
      if (p->name() == "profile") {
        setProfile = p->value();
        SetProfile();
      }
    }


    request->send(SPIFFS, "/index.html", String(), false, indexCb);

  });

  // napojeni kaskadovýyh stylu
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/style.css", "text/css");
  });

  // napojeni funkce aktualni teploty
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", getTemperature().c_str());
  });

  // napojeni funkce nastavene teploty
  server.on("/settedtemperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", getSettedTemperature().c_str());
  });

  // napojeni funkce nastavene teploty
  server.on("/huminidy", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", getHuminidy().c_str());
  });

  // status rele
  server.on("/isrelayon", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", isRelayOnString().c_str());
  });

  // napojeni funkce casu
  server.on("/time", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (setTime > 0) {
      timeDiff = setTime - (millis() / 1000) + secProfile;
    }
    else
    {
      timeDiff = 0;
    }
    request->send_P(200, "text/plain", getTime().c_str());
  });

  // napojeni funkce profilu
  server.on("/profile", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", getProfile().c_str());
  });

  // Start server
  server.begin();
}

void setStateDiode(int state) {
  switch (state) {
    case 0:
  digitalWrite(ledPinC1, LOW);
  digitalWrite(ledPinC2, LOW);
  digitalWrite(ledPinC3, HIGH);
      break;
    case 1:
  digitalWrite(ledPinC1, LOW);
  digitalWrite(ledPinC2, LOW);
  digitalWrite(ledPinC3, LOW);
      break;
    case 2:
  digitalWrite(ledPinC1, LOW);
  digitalWrite(ledPinC2, HIGH);
  digitalWrite(ledPinC3, LOW);
      break;
    case 3:
  digitalWrite(ledPinC1, HIGH);
  digitalWrite(ledPinC2, LOW);
  digitalWrite(ledPinC3, LOW);
      break;
    default:
  digitalWrite(ledPinC1, LOW);
  digitalWrite(ledPinC2, LOW);
  digitalWrite(ledPinC3, LOW);
      break;
  }
}

void loop() {
  int amill = millis();
  logToMySql(amill);
  if (amill - lastFiveSecMillis > 5000) {
    lastFiveSecMillis = amill;
    loadTemperature();
    Serial.print("Teplota v udirně ");
    Serial.print(curTemp);
    Serial.print(" °C Teplota venku ");
    Serial.print(curSecondTemp);
    Serial.print(" °C Naposledy změněná teplota při ");
    Serial.print(lastChangeTemp);
    Serial.print(" °C");
    Serial.println();
  }
}
