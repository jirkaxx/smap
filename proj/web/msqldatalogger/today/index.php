<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>moje UDIRNA | dnes</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        table{
            margin: 0px auto;
        }
        th,td {
            text-align: center;
            min-width: 200px;
        }
        th {
            font-size: 1.5em;
            background-color: #6D283F;
            color:#ffffff;
        }
        .big-tr{
            font-size: 1.5em;
            background-color: #6D283F !important;
            color: #ffffff !important;
        }
        tr:nth-child(even) {background: #e8e8e8}
        tr:nth-child(odd) {background: #ffffff}
        </style>
</head>
<body>
<table>
    <tr>
        <th>Čas</th>
        <th>Senzor</th>
        <th>Vlhkost</th>
        <th>Sepnutá spirála</th>
        <th>Nastavená teplota</th>
    </tr>
<?php
include "../dblogin.php";

echo "<tr class='big-tr'><td colspan='5'>" . date("j.n.Y") . "</td></tr>";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT datetime, degree, setdegree, huminidy, isrelayon FROM data WHERE DATE(datetime) = CURDATE()";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $dbdatetime = strtotime($row["datetime"]);
        $newdatetime = date('H:i:s',$dbdatetime);

        $relay = "Ano";
        if($row["isrelayon"] == 0){
            $relay = "Ne";
        }

        echo "<tr>
                 <td>".$newdatetime."</td>
                 <td>".$row["degree"]." °C</td>
                 <td>".$row["huminidy"]." %</td>
                 <td>".$relay."</td>
                 <td>".$row["setdegree"]." °C</td>
             </tr>";
    }
} else {
    echo "<tr><td colspan='5'>Žádný záznam</td></tr>";
}
$conn->close();
?>
</table>
</body>
</html>